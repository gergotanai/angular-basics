import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lifecycle-hooks',
  templateUrl: './lifecycle-hooks.component.html',
  styleUrls: ['./lifecycle-hooks.component.scss']
})
export class LifecycleHooksComponent implements OnInit {

  private number: number = 500;
  name: string = 'Gergő value';
  isVisible: boolean = true;

  user = {
    name: 'Gergő reference'
  }

  get counter() {
    return this.number;
  }

  set counter(value) {
    this.number = value;
  }

  constructor() { }

  ngOnInit(): void {
  }

  increment(): void {
    this.counter++;
  }

  decrement(): void {
    this.counter--;
  }

  updateName(): void {
    // this.name = 'Gergő vagyok';
    this.user.name = 'Gergő vagyok';
  }

  switchVisibility(): void {
    this.isVisible = !this.isVisible;
  }
}
