import { Component, OnInit, Input, OnChanges, SimpleChange, SimpleChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {

  private myNumber: number;
  @Input() myName: any;

  get myNewNumber() {
    return this.myNumber;
  }

  @Input()
  set myNewNumber(value: number) {
    this.myNumber = value;
  }

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    console.log('ngOnChanges() called before ngOnInit().');
    console.log('Whenever any chanege comes from the parent component ngOnChanges() gets triggered.');
    console.log("ngOnChanges() gets called when values are passed by value, won't execute when values are passed by reference.");
    const newNumberChanged: SimpleChange = changes.myNewNumber;
    if (changes.myNewNumber !== undefined) {
      console.log('Previous value (ngOnChanges()): ', newNumberChanged.previousValue);
      console.log('Current value (ngOnChanges()): ', newNumberChanged.currentValue);
    }
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    console.log('ngOnInit() is called after ngOnChanges() and this lifecycle hook is only called once.');
    console.log('ngOnInit() value: ', this.myNewNumber);
  }

  ngDoCheck(): void {
    //Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
    //Add 'implements DoCheck' to the class.
    console.log('ngDoCheck() gets called after ngOnChanges() and ngOnInit().');
    console.log("ngDoCheck() gets called both when values are passed by value and reference.");
  }

  ngAfterContentInit(): void {
    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.
    console.log('ngAfterContentInit() gets called after ngOnChanges(), ngOnInit() and ngDoCheck(), this lifecycle hook is only called once.');
  }

  ngAfterContentChecked(): void {
    //Called after every check of the component's or directive's content.
    //Add 'implements AfterContentChecked' to the class.
    console.log('ngAfterContentChecked() gets called after every ngDoCheck().');
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    console.log("Called after ngAfterContentInit() when the component's view has been initialized. Applies to components only. Called only once.");
  }

  ngAfterViewChecked(): void {
    //Called after every check of the component's view. Applies to components only.
    //Add 'implements AfterViewChecked' to the class.
    console.log("Called after every check of the component's view. Applies to components only.");
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    console.log('Called once, before the instance is destroyed.');
  }
}
