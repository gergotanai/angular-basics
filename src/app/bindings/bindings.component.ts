import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bindings',
  templateUrl: './bindings.component.html',
  styleUrls: ['./bindings.component.scss']
})
export class BindingsComponent implements OnInit {

  wow: string = 'wow';

  constructor() { }

  ngOnInit(): void {
  }

  outputDateArrived(date: Date): void {
    document.getElementById('currentDate').innerHTML = date.toDateString();
  }

}
