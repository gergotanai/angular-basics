import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputEventEmitterComponent } from './output-event-emitter.component';

describe('OutputEventEmitterComponent', () => {
  let component: OutputEventEmitterComponent;
  let fixture: ComponentFixture<OutputEventEmitterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputEventEmitterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputEventEmitterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
