import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-output-event-emitter',
  templateUrl: './output-event-emitter.component.html',
  styleUrls: ['./output-event-emitter.component.scss']
})
export class OutputEventEmitterComponent implements OnInit {

  @Output() outputDate = new EventEmitter<Date>();
  date: Date = new Date();

  constructor() { }

  ngOnInit(): void {
  }

  passCurrentTime(): void {
    this.date.getTime();
    this.outputDate.emit(this.date);
  }

}
