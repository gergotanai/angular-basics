import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-local-references',
  templateUrl: './local-references.component.html',
  styleUrls: ['./local-references.component.scss']
})
export class LocalReferencesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  sayMyName(name: string) {
    if (name === '') return;
    document.getElementById('myName').innerHTML = 'Your name is: ' + name;
  }

}
