import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DirectivesComponent } from './directives/directives.component';
import { BindingsComponent } from './bindings/bindings.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ServicesComponentComponent } from './services-component/services-component.component';
import { HttpRequestsComponent } from './http-requests/http-requests.component';
import { LifecycleHooksComponent } from './lifecycle-hooks/lifecycle-hooks.component';


const routes: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'bindings', component: BindingsComponent },
  { path: 'directives', component:  DirectivesComponent},
  { path: 'services', component: ServicesComponentComponent},
  { path: 'httprequests', component: HttpRequestsComponent},
  { path: 'lifecycle_hooks', component: LifecycleHooksComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
