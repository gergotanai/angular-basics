import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-http-requests',
  templateUrl: './http-requests.component.html',
  styleUrls: ['./http-requests.component.scss']
})
export class HttpRequestsComponent implements OnInit {

  nameHeights: Map<string, string> = new Map();
  isLoading: boolean = true;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.fetchQuiz();
  }

  fetchQuiz(): void {
    this.http.get<any>('https://swapi.dev/api/people')
      .pipe(map(response => {
        let map = new Map();
        for (let i: number = 0; i < response.results.length; i++) {
          map.set(response.results.map(data => data.name)[i] ,response.results.map(data => data.height)[i]);
        }
        return map;
        }))
      .subscribe(transformedData => {
        this.nameHeights = transformedData;
        this.isLoading = false;
      });
  }
}
