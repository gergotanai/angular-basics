import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-class',
  templateUrl: './ng-class.component.html',
  styleUrls: ['./ng-class.component.scss']
})
export class NgClassComponent implements OnInit {

  date: Date = new Date();

  constructor() { }

  ngOnInit(): void {
  }

  setClasses() {
    let myClasses = {
      redColorStyle: this.date.getSeconds() % 2 === 0,
      greenBackgroundColorStyle: this.date.getSeconds() % 2 === 1
    }
    return myClasses;
  }

}
