import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgTemplateWithNgIfComponent } from './ng-template-with-ng-if.component';

describe('NgTemplateWithNgIfComponent', () => {
  let component: NgTemplateWithNgIfComponent;
  let fixture: ComponentFixture<NgTemplateWithNgIfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgTemplateWithNgIfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgTemplateWithNgIfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
