import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-template-with-ng-if',
  templateUrl: './ng-template-with-ng-if.component.html',
  styleUrls: ['./ng-template-with-ng-if.component.scss']
})
export class NgTemplateWithNgIfComponent implements OnInit {

  date: Date = new Date();

  constructor() { }

  ngOnInit(): void {
  }

}
