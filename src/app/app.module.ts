import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { InterpolationComponent } from './bindings/interpolation/interpolation.component';
import { PropertyBindingComponent } from './bindings/property-binding/property-binding.component';
import { NgForComponent } from './directives/ng-for/ng-for.component';
import { EventBindingComponent } from './bindings/event-binding/event-binding.component';
import { StyleBindingComponent } from './bindings/style-binding/style-binding.component';
import { NgStyleComponent } from './directives/ng-style/ng-style.component';
import { ClassBindingComponent } from './bindings/class-binding/class-binding.component';
import { NgClassComponent } from './directives/ng-class/ng-class.component';
import { NgModelComponent } from './directives/ng-model/ng-model.component';
import { LocalReferencesComponent } from './bindings/local-references/local-references.component';
import { NgTemplateWithNgIfComponent } from './directives/ng-template-with-ng-if/ng-template-with-ng-if.component';
import { OutputEventEmitterComponent } from './bindings/output-event-emitter/output-event-emitter.component';
import { DirectivesComponent } from './directives/directives.component';
import { BindingsComponent } from './bindings/bindings.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { PersonsComponent } from './services-component/services/components/persons/persons.component';
import { ServicesComponentComponent } from './services-component/services-component.component';
import { PersonInputComponent } from './services-component/services/components/person-input/person-input.component';
import { HttpRequestsComponent } from './http-requests/http-requests.component';
import { LifecycleHooksComponent } from './lifecycle-hooks/lifecycle-hooks.component';
import { ChildComponent } from './lifecycle-hooks/child/child.component';

@NgModule({
  declarations: [
    AppComponent,
    InterpolationComponent,
    PropertyBindingComponent,
    NgForComponent,
    EventBindingComponent,
    StyleBindingComponent,
    NgStyleComponent,
    ClassBindingComponent,
    NgClassComponent,
    NgModelComponent,
    LocalReferencesComponent,
    NgTemplateWithNgIfComponent,
    OutputEventEmitterComponent,
    DirectivesComponent,
    BindingsComponent,
    WelcomeComponent,
    PersonsComponent,
    ServicesComponentComponent,
    PersonInputComponent,
    HttpRequestsComponent,
    LifecycleHooksComponent,
    ChildComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
