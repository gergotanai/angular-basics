import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { ServiceService } from '../../service.service';

@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html',
  styleUrls: ['./persons.component.scss']
})
export class PersonsComponent implements OnInit, OnDestroy {

  personList: string[];
  private personListSubs: Subscription;

  constructor(private service: ServiceService) { }

  ngOnInit(): void {
    this.personList = this.service.persons;
    this.personListSubs = this.service.personsChanged.subscribe(persons => {
      this.personList = persons;
    });
  }

  ngOnDestroy(): void {
    this.personListSubs.unsubscribe();
  }

  onRemovePerson(name: string): void {
    this.service.removePerson(name);
  }
}
