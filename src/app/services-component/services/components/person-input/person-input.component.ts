import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../service.service';

@Component({
  selector: 'app-person-input',
  templateUrl: './person-input.component.html',
  styleUrls: ['./person-input.component.scss']
})
export class PersonInputComponent implements OnInit {

  enteredPersonName: string = '';

  constructor(private service: ServiceService) { }

  ngOnInit(): void {
  }

  onAddPerson(): void {
    if (this.enteredPersonName === '') return;
    this.service.addPerson(this.enteredPersonName);
    this.enteredPersonName = '';
  }
}
