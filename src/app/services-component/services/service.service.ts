import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  personsChanged = new Subject<string[]>();
  persons: string[] = ['Anna', 'Barbara', 'Camilla'];

  constructor() { }

  addPerson(name: string): void {
    this.persons.push(name);
    this.personsChanged.next(this.persons);
  }

  removePerson(name: string): void {
    this.persons = this.persons.filter(person => {
      return person !== name;
    });
    this.personsChanged.next(this.persons);
  }
}
